int_number = 1001
float_number = 15.23666666
string = 'I am a String!'

sample_list = [1, 2, 3, 'Item1', 'Item2']

print('Integer : %d' % int_number)
print('Decimal : %.2f' % float_number)
print('String : %s' % string)
print('List : %s' % sample_list)
