string = 'lorem IPSUM olor SIT amet'

print('First Character to capital letter : ', string.capitalize())
print('Lowercase : ', string.casefold())
print('Padding on both ends : ', string.center(50), '|')
print('Count the occurences of "o": ', string.count('o'))
print('Ending with et ? : ', string.endswith('et'))
