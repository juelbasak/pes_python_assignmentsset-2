# importing the time module
import time


try:
	# counting for a minute by 5 seconds interval gap
	for i in range(12):
	    print(f'{i*5} secs passed! ',time.strftime('%H:%M:%S', time.localtime()), end = '')
	    
	    print('\tWaiting', end='')
	    for i in range(5):
	        print('.', end = '')
	        time.sleep(1)
	    print()
	print('60 secs passed!')

	# storing an initial starting time 

	t1 = time.perf_counter_ns()

	# importing a programs module and executing it

	import prog_32

	# storing the execution end time

	t2 = time.perf_counter_ns()

	# calculating the difference of the end time and the start time

	print('Time taken : %.2f nanosecond' % (t2 - t1))


# handling any kind of errors that can occur

except ImportError:
	# if the files to be imported are not found
	print('Some files were not found!')

except Exception:
	# if any other exception occurred
	print('Some Error Occurred')
